// vite.config.js
import { resolve } from 'path';
import { defineConfig } from 'vite';

export default defineConfig({
    build: {
        rollupOptions: {
            input: {
                main: resolve(__dirname, 'index.html'),
                products: resolve(__dirname, 'products/index.html'),
                orders: resolve(__dirname, 'orders/index.html'),
                admin: resolve(__dirname, 'admin/index.html'),
                adminOrders: resolve(__dirname, 'admin/orders/index.html'),
                adminProducts: resolve(__dirname, 'admin/products/index.html'),
            },
        },
    },
});
