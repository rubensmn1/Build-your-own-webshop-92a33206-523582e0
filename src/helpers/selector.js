export default function selectAndModify(parent, selectorString, props = {}) {
    const element = parent.querySelector(selectorString);
    if (element === null || element === undefined) {
        throw new Error(
            `Element not found with selector of: ${selectorString}`,
        );
    }
    Object.entries(props).forEach(([key, value]) => {
        element[key] = value;
    });
    return element;
}
