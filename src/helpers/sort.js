export function sortNumbersAscending(a, b) {
    return a - b;
}

export function sortNumbersDescending(a, b) {
    return b - a;
}

export function sortStringsAscending(a, b) {
    return a.localeCompare(b);
}

export function sortStringsDescending(a, b) {
    return b.localeCompare(a);
}
