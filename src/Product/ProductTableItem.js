import EventManager, { EVENT_TYPES } from '../EventManager';
import selectAndModify from '../helpers/selector';
import ProductStorage from '../Storage/ProductStorage';

const template = document.getElementById('productTableTemplate');

class ProductTableItem {
    parent = null;
    element = null;
    product = null;
    hasRendered = false;

    constructor(parent, product) {
        this.parent = parent;
        this.element = template.content.firstElementChild.cloneNode(true);
        this.parent.appendChild(this.element);

        // set the product
        this.product = product;
    }

    handleEdit(data) {
        // parse numbers
        data.cost = parseFloat(data.cost);
        data.stock = parseFloat(data.stock);

        // create new product with updated values
        const updatedProduct = {
            ...this.product,
            ...data,
        };

        // sync with the store and rerender
        const updatedProducts = ProductStorage.updateItem(updatedProduct);
        EventManager.notify(EVENT_TYPES.PRODUCTS_UPDATED, updatedProducts);
    }

    render() {
        selectAndModify(this.element, '.product-name', {
            innerText: this.product.name,
        });

        selectAndModify(this.element, '.product-stock', {
            innerText: this.product.stock,
        });

        selectAndModify(this.element, '.product-cost', {
            innerText: this.product.cost,
        });

        const editButton = selectAndModify(this.element, '.product-edit');
        // only add event listener if not rendered yet
        if (this.hasRendered === false) {
            editButton.addEventListener('click', () => {
                // get the dialog
                const dialog = document.getElementById('dialog');

                // update the header
                selectAndModify(dialog, 'h2', {
                    innerText: `Edit - ${this.product.name}`,
                });

                // update the fields with the correct values
                selectAndModify(dialog, "input[name='name']", {
                    value: this.product.name,
                });

                selectAndModify(dialog, "input[name='cost']", {
                    value: this.product.cost,
                });

                selectAndModify(dialog, "input[name='stock']", {
                    value: this.product.stock,
                });

                selectAndModify(dialog, 'button:last-of-type', {
                    innerText: 'Save',
                });

                const form = selectAndModify(dialog, 'form');

                // handle the close of the dialog
                const handleClose = () => {
                    if (dialog.returnValue) {
                        const formData = new FormData(form);
                        const data = Object.fromEntries(formData.entries());
                        this.handleEdit(data);
                    }
                    // clear form
                    form.reset();
                    // remove listener
                    dialog.removeEventListener('close', handleClose);
                };

                dialog.addEventListener('close', handleClose);

                // show the modal
                dialog.showModal();
            });
        }

        const deleteButton = selectAndModify(this.element, '.product-delete');
        // only add event listener if not rendered yet
        if (this.hasRendered === false) {
            deleteButton.addEventListener('click', () => {
                ProductStorage.removeById(this.product.id);
                EventManager.notify(EVENT_TYPES.PRODUCT_DELETED, this.product);
                this.element.remove();
            });
        }

        if (this.hasRendered === false) this.hasRendered = true;
    }
}

export default ProductTableItem;
