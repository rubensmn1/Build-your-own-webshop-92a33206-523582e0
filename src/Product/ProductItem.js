import Cart from '../Cart';
import selectAndModify from '../helpers/selector';

const productTemplate = document.getElementById('productTemplate');

class ProductItem {
    product = null;
    hasRendered = false;

    constructor(parent, product) {
        this.product = product;

        this.parent = parent;

        // set the element to the template clone
        this.element =
            productTemplate.content.firstElementChild.cloneNode(true);
        // append the element to the list
        parent.appendChild(this.element);
    }

    render() {
        // add data for link - optional
        const linkElement = this.element.querySelector('a');
        if (linkElement) linkElement.href = `/products/#${this.product.id}`;

        selectAndModify(this.element, 'h3', {
            innerText: this.product.name,
        });

        // add data for price
        selectAndModify(this.element, 'p', {
            innerText: `€${this.product.cost.toFixed(2)}`,
        });

        // add logic for add to cart button
        const addToCartButton = selectAndModify(
            this.element,
            "button[type='submit']",
        );
        if (this.hasRendered === false) {
            addToCartButton.addEventListener('click', (e) => {
                e.preventDefault();
                Cart.add(this.product, input.valueAsNumber);
                input.valueAsNumber = 1;
                input.dispatchEvent(new Event('change'));
            });
        }

        const input = selectAndModify(this.element, 'input', {
            max: this.product.stock,
        });
        if (this.hasRendered === false) {
            input.addEventListener('change', (e) => {
                const currentValue = e.currentTarget.valueAsNumber;
                if (currentValue >= this.product.stock) {
                    e.currentTarget.valueAsNumber = this.product.stock;
                    incrementButton.disabled = true;
                } else {
                    incrementButton.disabled = false;
                }
                if (currentValue <= 1) {
                    e.currentTarget.valueAsNumber = 1;
                    decrementButton.disabled = true;
                } else {
                    decrementButton.disabled = false;
                }
            });
        }

        const decrementButton = selectAndModify(
            this.element,
            '.decrement-btn',
            {
                disabled: true,
            },
        );
        if (this.hasRendered === false) {
            decrementButton.addEventListener('click', (e) => {
                e.preventDefault();
                input.valueAsNumber -= 1;
                input.dispatchEvent(new Event('change'));
            });
        }

        const incrementButton = selectAndModify(this.element, '.increment-btn');
        if (this.hasRendered === false) {
            incrementButton.addEventListener('click', (e) => {
                e.preventDefault();
                input.valueAsNumber += 1;
                input.dispatchEvent(new Event('change'));
            });
        }

        if (this.hasRendered === false) this.hasRendered = true;
    }
}

export default ProductItem;
