class Product {
    id = 'xxx';
    name = 'product';
    cost = 0;
    stock = 0;

    constructor(data) {
        this.id = data.id;
        this.name = data.name;
        this.cost = data.cost;
        this.stock = data.stock;
    }

    static fromData({ name, cost, stock }) {
        const id = Math.random().toString(16).slice(2);
        return new Product({
            id,
            name,
            cost,
            stock,
        });
    }
}

export default Product;
