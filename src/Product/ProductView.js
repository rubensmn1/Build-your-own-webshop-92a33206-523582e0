import EventManager, { EVENT_TYPES } from '../EventManager';
import { FILTER_TYPES } from '../Filters';
import {
    sortNumbersAscending,
    sortNumbersDescending,
    sortStringsAscending,
    sortStringsDescending,
} from '../helpers/sort';
import ProductItem from './ProductItem';

class ProductView {
    parent = null;
    products = [];
    sortedProducts = new Map();
    filter = 'default';
    DetailComponent = null;
    productsElements = new Map();

    constructor(parent, detailComponent = ProductItem) {
        this.parent = parent;
        this.DetailComponent = detailComponent;

        EventManager.subscribe(
            EVENT_TYPES.PRODUCT_SORT_CHANGED,
            ({ filter, propToSort, sortType }) => {
                if (filter !== this.filter) {
                    this.filter = filter;
                    this.sort(propToSort, sortType);
                }
            },
        );

        EventManager.subscribe(EVENT_TYPES.PRODUCTS_UPDATED, (products) => {
            this.setProducts(products);
        });

        EventManager.subscribe(EVENT_TYPES.PRODUCT_DELETED, (product) => {
            this.productsElements.delete(product.id);
        });
    }

    setProducts(products) {
        this.products = products;
        this.sortedProducts = new Map();
        // loop over all the products
        for (const product of products) {
            // add new product element to the map
            this.productsElements.set(
                product.id,
                new this.DetailComponent(this.parent, product),
            );
        }

        this.filter = 'default';
        this.sortedProducts.set(
            this.filter,
            products.map((p) => p.id),
        );

        EventManager.notify(EVENT_TYPES.PRODUCT_SORT_CHANGED, {
            filter: this.filter,
        });

        this.render();
    }

    sort(propToSort, sortType) {
        if (this.sortedProducts.has(propToSort) === false) {
            const sorted = this.products
                .sort((a, b) => {
                    switch (sortType) {
                        case FILTER_TYPES.STRING_ASCENDING:
                            return sortStringsAscending(
                                a[propToSort],
                                b[propToSort],
                            );
                        case FILTER_TYPES.STRING_DESCENDING:
                            return sortStringsDescending(
                                a[propToSort],
                                b[propToSort],
                            );
                        case FILTER_TYPES.NUMBER_ASCENDING:
                            return sortNumbersAscending(
                                a[propToSort],
                                b[propToSort],
                            );
                        case FILTER_TYPES.NUMBER_DESCENDING:
                            return sortNumbersDescending(
                                a[propToSort],
                                b[propToSort],
                            );
                        default:
                            return a[propToSort] - b[propToSort];
                    }
                })
                .map((product) => product.id);
            this.sortedProducts.set(this.filter, sorted);
        }
        this.render();
    }

    removeProduct(id) {
        const sorted = this.sortedProducts.get(this.filter);
        this.sortedProducts.set(
            this.filter,
            sorted.filter((product) => product.id !== id),
        );
    }

    render() {
        this.parent.replaceChildren();
        for (const productId of this.sortedProducts.get(this.filter)) {
            const productElement = this.productsElements.get(productId);
            if (productElement) {
                this.parent.appendChild(productElement.element);
                productElement.render();
            } else {
                this.removeProduct(productId);
            }
        }
    }
}

export default ProductView;
