import EventManager, { EVENT_TYPES } from '../EventManager';
import { FILTER_TYPES } from '../Filters';
import {
    sortNumbersAscending,
    sortNumbersDescending,
    sortStringsAscending,
    sortStringsDescending,
} from '../helpers/sort';
import OrderTableItem from './OrderTableItem';

class OrderView {
    element = null;
    filter = 'default';
    orders = [];
    sortedOrders = new Map();
    ComponentToRender = null;

    constructor(element, componentToRender = OrderTableItem) {
        this.element = element;
        this.ComponentToRender = componentToRender;

        EventManager.subscribe(
            EVENT_TYPES.PRODUCT_SORT_CHANGED,
            ({ filter, propToSort, sortType }) => {
                if (filter !== this.filter) {
                    this.filter = filter;
                    this.sort(propToSort, sortType);
                }
            },
        );
    }

    setOrders(orders) {
        this.orders = orders;
        this.sortedOrders.set('default', [...orders]);
        this.render();
    }

    sort(propToSort, sortType) {
        if (this.sortedOrders.has(propToSort) === false) {
            const sorted = this.orders.sort((a, b) => {
                switch (sortType) {
                    case FILTER_TYPES.STRING_ASCENDING:
                        return sortStringsAscending(
                            a[propToSort],
                            b[propToSort],
                        );
                    case FILTER_TYPES.STRING_DESCENDING:
                        return sortStringsDescending(
                            a[propToSort],
                            b[propToSort],
                        );
                    case FILTER_TYPES.NUMBER_ASCENDING:
                        return sortNumbersAscending(
                            a[propToSort],
                            b[propToSort],
                        );
                    case FILTER_TYPES.NUMBER_DESCENDING:
                        return sortNumbersDescending(
                            a[propToSort],
                            b[propToSort],
                        );
                    default:
                        return a[propToSort] - b[propToSort];
                }
            });
            this.sortedOrders.set(this.filter, sorted);
        }
        this.render();
    }

    render() {
        this.element.replaceChildren();
        for (const order of this.sortedOrders.get(this.filter)) {
            const orderTableItem = new this.ComponentToRender(
                this.element,
                order,
            );
            orderTableItem.render();
        }
    }
}

export default OrderView;
