import selectAndModify from '../helpers/selector';

const template = document.getElementById('orderItemTemplate');

class OrderItem {
    parent = null;
    element = null;
    orderItem = null;

    constructor(parent, orderItem) {
        this.parent = parent;
        this.element = template.content.firstElementChild.cloneNode(true);
        this.parent.appendChild(this.element);

        // set the order
        this.orderItem = orderItem;
    }

    render() {
        selectAndModify(this.element, '.order-product-id', {
            innerText: this.orderItem.id,
        });

        selectAndModify(this.element, '.order-product-name', {
            innerText: this.orderItem.name,
        });

        selectAndModify(this.element, '.order-item-quantity', {
            innerText: this.orderItem.quantity,
        });

        selectAndModify(this.element, '.order-item-subtotal', {
            innerText: this.orderItem.subtotal.toFixed(2),
        });
    }
}

export default OrderItem;
