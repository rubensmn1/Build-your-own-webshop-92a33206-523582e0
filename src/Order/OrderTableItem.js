import selectAndModify from '../helpers/selector';

const template = document.getElementById('orderTableTemplate');

class OrderTableItem {
    parent = null;
    element = null;
    order = null;

    constructor(parent, order) {
        this.parent = parent;
        this.element = template.content.firstElementChild.cloneNode(true);
        this.parent.appendChild(this.element);

        // set the order
        this.order = order;
    }

    render() {
        selectAndModify(this.element, '.order-id', {
            innerText: this.order.id,
        });

        selectAndModify(this.element, '.order-amount', {
            innerText: this.order.total,
        });

        selectAndModify(this.element, '.order-date', {
            innerText: new Date(this.order.date).toLocaleDateString(),
        });

        selectAndModify(this.element, '.order-actions a', {
            href: `/admin/orders/#${this.order.id}`,
        });
    }
}

export default OrderTableItem;
