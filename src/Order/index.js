class Order {
    id = null;
    items = [];

    constructor(items) {
        this.items = [...items];
        this.id = Math.random().toString(16).slice(2);
        this.total = this.items
            .reduce(
                (total, item) => total + item.product.cost * item.quantity,
                0,
            )
            .toFixed(2);
        this.date = new Date().toISOString();
    }
}

export default Order;
