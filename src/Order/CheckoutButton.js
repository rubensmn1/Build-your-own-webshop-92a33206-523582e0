import Order from '.';
import Cart from '../Cart';
import OrderStorage from '../Storage/OrderStorage';

class CheckoutButton {
    element = null;

    constructor(element) {
        this.element = element;
        element.addEventListener('click', () => {
            const newOrder = new Order(Cart.items);

            OrderStorage.addOrder(newOrder);
            Cart.removeAll();
            window.location.assign(`/orders/#${newOrder.id}`);
        });
    }
}

export default CheckoutButton;
