import EventManager, { EVENT_TYPES } from '../EventManager';

const template = document.createElement('template');
template.innerHTML = `<button
  class="flex
  justify-center
  items-center
  gap-1
  px-2
  py-1
  flex-1
  text-green-700
  rounded-md
  hover:bg-green-700
  hover:text-white
  ease-linear
  duration-150"
>
  A-Z
</button>`;

class FilterButton {
    element = null;
    filter = null;
    isActive = false;

    constructor(content, propToSort, sortType) {
        // set the element to the template clone
        this.element = template.content.firstElementChild.cloneNode(true);
        // add the content
        this.element.innerHTML = content;
        this.filter = content;

        // add click listener for the EventManager
        this.element.addEventListener('click', () => {
            EventManager.notify(EVENT_TYPES.PRODUCT_SORT_CHANGED, {
                filter: this.filter,
                propToSort,
                sortType,
            });
        });

        // subscribe to change event to update button state
        EventManager.subscribe(
            EVENT_TYPES.PRODUCT_SORT_CHANGED,
            ({ filter }) => {
                if (filter !== this.filter) {
                    if (this.isActive) {
                        this.isActive = false;
                        this.updateState();
                    }
                } else if (!this.isActive) {
                    this.isActive = true;
                    this.updateState();
                }
            },
        );
    }

    updateState() {
        if (this.isActive) {
            this.element.classList.add('bg-green-700');
            this.element.classList.add('text-white');
        } else {
            this.element.classList.remove('bg-green-700');
            this.element.classList.remove('text-white');
        }
    }
}

export default FilterButton;
