class Filters {
    parent = null;
    constructor(parent) {
        this.parent = parent;
    }

    addButton(filterButton) {
        if (Array.isArray(filterButton)) {
            for (const button of filterButton) {
                this.parent.appendChild(button.element);
            }
        } else {
            this.parent.appendChild(filterButton.element);
        }
    }
}

export const FILTER_TYPES = {
    STRING_ASCENDING: 'STRING_ASCENDING',
    STRING_DESCENDING: 'STRING_DESCENDING',
    NUMBER_ASCENDING: 'NUMBER_ASCENDING',
    NUMBER_DESCENDING: 'NUMBER_DESCENDING',
};

export default Filters;
