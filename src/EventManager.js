class EventManager {
    listeners = {};

    subscribe(type, listener) {
        if (!(type in this.listeners)) this.listeners[type] = [];
        this.listeners[type].push(listener);

        return () => this.unsubscribe(type, listener);
    }

    unsubscribe(type, listener) {
        this.listeners[type] = this.listeners[type].filter(
            (l) => l === listener,
        );
    }

    notify(type, data) {
        if (!(type in this.listeners)) return;
        for (const listener of this.listeners[type]) {
            listener(data);
        }
    }
}

export const EVENT_TYPES = {
    CART_ITEM_ADDED: 'CART_ITEM_ADDED',
    CART_ITEM_REMOVED: 'CART_ITEM_REMOVED',
    CART_TOTAL_ITEMS_UPDATED: 'CART_TOTAL_ITEMS_UPDATED',
    CART_TOTAL_PRICE_UPDATED: 'CART_TOTAL_PRICE_UPDATED',
    CART_ITEMS_LOADED: 'CART_ITEMS_LOADED',
    CART_ITEMS_REMOVED: 'CART_ITEMS_REMOVED',
    PRODUCT_SORT_CHANGED: 'PRODUCT_SORT_CHANGED',
    PRODUCT_DELETED: 'PRODUCT_DELETED',
    PRODUCTS_UPDATED: 'PRODUCTS_UPDATED',
};

const eventManager = new EventManager();
Object.freeze(eventManager);

export default eventManager;
