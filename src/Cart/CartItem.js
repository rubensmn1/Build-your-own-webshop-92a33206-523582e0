import selectAndModify from '../helpers/selector';
import Cart from '.';

// get the template
const template = document.getElementById('cartItemTemplate');

class CartItem {
    cartProduct = null;
    element = null;
    hasRendered = false;

    constructor(cartProduct, parent) {
        this.cartProduct = cartProduct;
        // set the element to the template clone
        this.element = template.content.firstElementChild.cloneNode(true);
        // append the element to the list
        parent.appendChild(this.element);
    }

    incrementQuantity = (_e, amount = 1) => {
        if (this.cartProduct.quantity < this.cartProduct.product.stock) {
            this.cartProduct.quantity += amount;
            Cart.updateItemQuantity(
                this.cartProduct,
                this.cartProduct.quantity,
            );
            this.render();
        }
    };

    decrementQuantity = (_e, amount = 1) => {
        if (this.cartProduct.quantity > 1) {
            this.cartProduct.quantity -= amount;
            Cart.updateItemQuantity(
                this.cartProduct,
                this.cartProduct.quantity,
            );
            this.render();
        }
    };

    updateQuantity = (amount) => {
        this.cartProduct.quantity = amount;
        Cart.updateItemQuantity(this.cartProduct, this.cartProduct.quantity);
        this.render();
    };

    removeItem = () => {
        // remove the item from the cart
        Cart.removeById(this.cartProduct.product.id);
        // remove the actual element from the dom
        this.element.remove();
    };

    remount(parent) {
        parent.appendChild(this.element);
    }

    // renders the element
    render() {
        selectAndModify(this.element, 'a', {
            innerText: this.cartProduct.product.name,
            href: `/products/#${this.cartProduct.product.id}`,
        });

        const input = selectAndModify(this.element, 'input', {
            value: this.cartProduct.quantity,
        });
        if (this.hasRendered === false) {
            input.addEventListener('change', (e) => {
                const newQuantity = e.currentTarget.valueAsNumber;
                this.updateQuantity(newQuantity);
            });
        }

        const decrementButton = selectAndModify(this.element, '.decrement-btn');
        if (this.hasRendered === false) {
            decrementButton.addEventListener('click', this.decrementQuantity);
        }

        const incrementButton = selectAndModify(this.element, '.increment-btn');
        if (this.hasRendered === false) {
            incrementButton.addEventListener('click', this.incrementQuantity);
        }

        selectAndModify(this.element, 'p:last-of-type', {
            innerText: `€${(
                this.cartProduct.product.cost * this.cartProduct.quantity
            ).toFixed(2)}`,
        });

        const button = selectAndModify(this.element, 'button.delete');
        if (this.hasRendered === false) {
            button.addEventListener('click', this.removeItem);
        }

        if (this.hasRendered === false) this.hasRenderd = true;
    }
}

export default CartItem;
