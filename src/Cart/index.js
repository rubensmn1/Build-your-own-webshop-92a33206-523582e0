import EventManager, { EVENT_TYPES } from '../EventManager';
import CartStorage from '../Storage/CartStorage';
import CartProduct from './CartProduct';

class Cart {
    items = [];
    storage;

    constructor() {
        this.storage = new CartStorage();
        this.items = this.storage.get();
    }

    add(product, quantity = 1) {
        let cartProduct = this.items.find(
            (item) => item.product.id === product.id,
        );

        if (cartProduct) {
            cartProduct.quantity += quantity;
        } else {
            cartProduct = new CartProduct(product, quantity);

            this.items.push(cartProduct);
        }
        this.storage.set(this.items);
        EventManager.notify(EVENT_TYPES.CART_ITEM_ADDED, cartProduct);
        EventManager.notify(
            EVENT_TYPES.CART_TOTAL_ITEMS_UPDATED,
            this.getTotalItems(),
        );
        EventManager.notify(
            EVENT_TYPES.CART_TOTAL_PRICE_UPDATED,
            this.getTotalPrice(),
        );
    }

    updateItemQuantity(itemToUpdate, quantity) {
        this.items = this.items.map((item) => {
            if (item.product.id !== itemToUpdate.id) return item;
            item.quantity = quantity;
            return item;
        });

        this.storage.set(this.items);

        EventManager.notify(
            EVENT_TYPES.CART_TOTAL_ITEMS_UPDATED,
            this.getTotalItems(),
        );
        EventManager.notify(
            EVENT_TYPES.CART_TOTAL_PRICE_UPDATED,
            this.getTotalPrice(),
        );
    }

    removeById(id) {
        this.items = this.items.filter((item) => item.product.id !== id);

        this.storage.set(this.items);

        EventManager.notify(EVENT_TYPES.CART_ITEM_REMOVED, id);
        EventManager.notify(
            EVENT_TYPES.CART_TOTAL_ITEMS_UPDATED,
            this.getTotalItems(),
        );
        EventManager.notify(
            EVENT_TYPES.CART_TOTAL_PRICE_UPDATED,
            this.getTotalPrice(),
        );
    }

    removeAll() {
        this.items = [];
        EventManager.notify(EVENT_TYPES.CART_ITEMS_REMOVED);
        EventManager.notify(
            EVENT_TYPES.CART_TOTAL_ITEMS_UPDATED,
            this.getTotalItems(),
        );
        EventManager.notify(
            EVENT_TYPES.CART_TOTAL_PRICE_UPDATED,
            this.getTotalPrice(),
        );
        this.storage.set(this.items);
    }

    getTotalPrice() {
        return this.items.reduce(
            (total, item) => total + item.product.cost * item.quantity,
            0,
        );
    }

    getTotalItems() {
        return this.items.reduce((total, item) => total + item.quantity, 0);
    }
}

const cart = new Cart();

export default cart;
