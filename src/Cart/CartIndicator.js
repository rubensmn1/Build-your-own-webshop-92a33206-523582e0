import Cart from '.';
import EventManager, { EVENT_TYPES } from '../EventManager';

class CartIndicator {
    element = null;
    total = 0;

    constructor(element) {
        this.element = element;
        this.total = Cart.getTotalItems();
        this.render();

        EventManager.subscribe(
            EVENT_TYPES.CART_TOTAL_ITEMS_UPDATED,
            (newTotal) => {
                this.total = newTotal;
                this.render();
            },
        );
    }

    render() {
        if (this.total > 99) {
            // limit to 99
            this.element.innerText = `+99`;
        } else {
            this.element.innerText = this.total;
        }
    }
}

export default CartIndicator;
