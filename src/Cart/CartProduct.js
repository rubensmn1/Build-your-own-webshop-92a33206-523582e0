class CartProduct {
    product = null;
    quantity = 0;

    constructor(product, quantity) {
        this.product = product;
        this.quantity = quantity;
    }
}

export default CartProduct;
