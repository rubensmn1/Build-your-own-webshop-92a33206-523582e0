import Cart from '.';
import EventManager, { EVENT_TYPES } from '../EventManager';
import selectAndModify from '../helpers/selector';
import CartItem from './CartItem';

class CartView {
    element = null;
    cartItems = new Map();
    totalPrice = 0;

    constructor(element) {
        this.element = element;
        this.totalPrice = Cart.getTotalPrice();
        this.renderTotal();

        // get and add items for the cart
        for (const item of Cart.items) {
            this.addItem(item);
        }

        // setup events
        EventManager.subscribe(EVENT_TYPES.CART_ITEM_ADDED, (newItem) => {
            this.addItem(newItem);
            this.renderTotal();
        });

        EventManager.subscribe(
            EVENT_TYPES.CART_TOTAL_PRICE_UPDATED,
            (newTotal) => {
                this.totalPrice = newTotal;
                this.renderTotal();
            },
        );

        EventManager.subscribe(
            EVENT_TYPES.CART_ITEM_REMOVED,
            (itemIdToRemove) => {
                this.removeItem(itemIdToRemove);
            },
        );

        EventManager.subscribe(EVENT_TYPES.CART_ITEMS_LOADED, () => {
            this.renderItems();
        });

        EventManager.subscribe(EVENT_TYPES.CART_ITEMS_REMOVED, () => {
            this.clearItems();
        });
    }

    addItem(item) {
        // check if the item already exists
        if (this.cartItems.has(item.product.id)) {
            this.cartItems.get(item.product.id).updateQuantity(item.quantity);
        } else {
            // create the new cart item
            const newCartItem = new CartItem(item, this.element);
            this.cartItems.set(item.product.id, newCartItem);

            // render the new cart item
            newCartItem.render();
        }
    }

    removeItem(itemIdToRemove) {
        this.cartItems.delete(itemIdToRemove);
    }

    clearItems() {
        this.items = {};
        this.element.replaceChildren();
    }

    renderItems() {
        this.cartItems.forEach((item) => {
            item.render();
        });
    }

    renderTotal() {
        selectAndModify(document.getElementById('cart'), '#cartTotal', {
            innerText: `€${this.totalPrice.toFixed(2)}`,
        });
    }
}

export default CartView;
