class LocalStorage {
    localStorageKey = 'my-webshop';

    constructor(name) {
        this.localStorageKey += `-${name}`;
    }

    get(defaultData = '{}') {
        const json = localStorage.getItem(this.localStorageKey) ?? defaultData;
        const data = JSON.parse(json);
        return data;
    }

    set(data) {
        localStorage.setItem(this.localStorageKey, JSON.stringify(data));
    }
}

export default LocalStorage;
