import LocalStorage from './LocalStorage';

class OrderStorage extends LocalStorage {
    constructor() {
        super('orders');
    }

    get() {
        return super.get('[]');
    }

    addOrder(newOrder) {
        const orders = this.get();
        this.set([...orders, newOrder]);
    }

    getById(id) {
        const orders = this.get();
        return orders.find((order) => order.id === id);
    }
}

const orderStorage = new OrderStorage();
Object.freeze(orderStorage);

export default orderStorage;
