import getInitialProducts from '../helpers/data';
import LocalStorage from './LocalStorage';

class ProductStorage extends LocalStorage {
    constructor() {
        super('products');
    }

    get() {
        return super.get('[]');
    }

    setItem(item) {
        const data = this.get();
        data.push(item);
        this.set(data);
        return data;
    }

    updateItem(itemToUpdate) {
        const data = this.get();
        const updatedData = data.map((item) => {
            if (item.id !== itemToUpdate.id) return item;
            return itemToUpdate;
        });
        this.set(updatedData);
        return updatedData;
    }

    getItemById(id) {
        const data = this.get();
        return data.find((product) => product.id === id);
    }

    removeById(id) {
        const data = this.get();
        const updatedData = data.filter((item) => item.id !== id);
        this.set(updatedData);
        return updatedData;
    }

    async reset() {
        const originalData = await getInitialProducts();
        this.set(originalData);
        return this.get();
    }
}

const productStorage = new ProductStorage();
Object.freeze(productStorage);

export default productStorage;
