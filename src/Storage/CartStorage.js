import LocalStorage from './LocalStorage';

class CartStorage extends LocalStorage {
    constructor() {
        super('cart');
    }

    get() {
        return super.get('[]');
    }

    getItemById(id) {
        const data = this.get();
        return data.find((item) => item.product.id === id);
    }
}

export default CartStorage;
