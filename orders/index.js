import OrderStorage from '../src/Storage/OrderStorage';

const itemTemplate = document.querySelector('template');
const tableBody = document.querySelector('tbody');
const orderTotalElement = document.getElementById('orderTotal');
const orderNumberElement = document.getElementById('orderNumber');

// get the product if from the url
const orderId = window.location.hash.slice(1);

// get the product from localstorage and create a Product class from the data
const order = OrderStorage.getById(orderId);

// set the title of the document
document.title += ` ${order.id}`;

function renderItem(item) {
    const itemElement = itemTemplate.content.firstElementChild.cloneNode(true);
    const dataFields = itemElement.querySelectorAll('td');
    dataFields[0].innerText = item.product.name;
    dataFields[1].innerText = item.quantity;
    dataFields[2].innerText = (item.quantity * item.product.cost).toFixed(2);
    tableBody.appendChild(itemElement);
}

// loop over all the items of the order
for (const item of order.items) {
    renderItem(item);
}

// set the meta data to the dom
orderNumberElement.innerText = order.id;
orderTotalElement.innerText = `€${order.total}`;
