import Filters, { FILTER_TYPES } from '../src/Filters';
import FilterButton from '../src/Filters/FilterButton';
import OrderView from '../src/Order/OrderView';
import OrderStorage from '../src/Storage/OrderStorage';

const orders = OrderStorage.get();

const orderView = new OrderView(document.querySelector('tbody'));

orderView.setOrders(orders);

const filters = new Filters(document.getElementById('filters'));
const priceAscending = new FilterButton(
    `Amount<ion-icon name="chevron-down-outline"></ion-icon>`,
    'total',
    FILTER_TYPES.NUMBER_ASCENDING,
);
const priceDescending = new FilterButton(
    `Amount<ion-icon name="chevron-up-outline"></ion-icon>`,
    'total',
    FILTER_TYPES.NUMBER_DESCENDING,
);
const nameAscending = new FilterButton(
    `Date<ion-icon name="chevron-down-outline"></ion-icon>`,
    'date',
    FILTER_TYPES.STRING_ASCENDING,
);
const nameDescending = new FilterButton(
    `Date<ion-icon name="chevron-up-outline"></ion-icon>`,
    'date',
    FILTER_TYPES.STRING_DESCENDING,
);
filters.addButton([
    priceAscending,
    priceDescending,
    nameAscending,
    nameDescending,
]);
