import EventManager, { EVENT_TYPES } from '../../src/EventManager';
import Filters, { FILTER_TYPES } from '../../src/Filters';
import FilterButton from '../../src/Filters/FilterButton';
import selectAndModify from '../../src/helpers/selector';
import Product from '../../src/Product/Product';
import ProductTableItem from '../../src/Product/ProductTableItem';
import ProductView from '../../src/Product/ProductView';
import ProductStorage from '../../src/Storage/ProductStorage';

// setup the product view
const productView = new ProductView(
    document.querySelector('tbody'),
    ProductTableItem,
);

// create the filters
const filters = new Filters(document.getElementById('filters'));
const nameAscending = new FilterButton(
    `A-Z`,
    'name',
    FILTER_TYPES.STRING_ASCENDING,
);
const nameDescending = new FilterButton(
    `Z-A`,
    'name',
    FILTER_TYPES.STRING_DESCENDING,
);
const stockAscending = new FilterButton(
    `Stock<ion-icon name="chevron-down-outline"></ion-icon>`,
    'stock',
    FILTER_TYPES.NUMBER_ASCENDING,
);
const stockDescending = new FilterButton(
    `Stock<ion-icon name="chevron-up-outline"></ion-icon>`,
    'stock',
    FILTER_TYPES.NUMBER_DESCENDING,
);
const costAscending = new FilterButton(
    `Cost<ion-icon name="chevron-down-outline"></ion-icon>`,
    'cost',
    FILTER_TYPES.NUMBER_ASCENDING,
);
const costDescending = new FilterButton(
    `Cost<ion-icon name="chevron-up-outline"></ion-icon>`,
    'cost',
    FILTER_TYPES.NUMBER_DESCENDING,
);
filters.addButton([
    nameAscending,
    nameDescending,
    stockAscending,
    stockDescending,
    costAscending,
    costDescending,
]);

// dialog logic
const dialog = document.getElementById('dialog');
document.getElementById('newProductButton').addEventListener('click', () => {
    if (!dialog.open) {
        dialog.showModal();
    } else {
        dialog.close();
    }

    selectAndModify(dialog, 'h2', {
        innerText: `Add a new product`,
    });

    const form = document.querySelector('form');

    function handleModalClose() {
        if (dialog.returnValue) {
            const formData = new FormData(form);
            // transform form data into a object
            const data = Object.fromEntries(formData.entries());
            // parse the numbers
            data.cost = parseFloat(data.cost);
            data.stock = parseFloat(data.stock);
            // create a new product
            const product = Product.fromData(data);
            const updatedProducts = ProductStorage.setItem(product);
            // reset the form
            form.reset();

            EventManager.notify(EVENT_TYPES.PRODUCTS_UPDATED, updatedProducts);
        }

        dialog.removeEventListener('close', handleModalClose);
    }

    document
        .getElementById('closeModalButton')
        .addEventListener('click', () => {
            dialog.close();
            dialog.removeEventListener('close', handleModalClose);
        });

    dialog.addEventListener('close', handleModalClose);
});

document
    .getElementById('resetProductsButton')
    .addEventListener('click', async () => {
        const originalProducts = await ProductStorage.reset();
        EventManager.notify(EVENT_TYPES.PRODUCTS_UPDATED, originalProducts);
    });

async function init() {
    // get the product from storage
    let products = ProductStorage.get();
    // check if there are products if not, reset to default products
    if (products.length < 1) {
        products = await ProductStorage.reset();
    }

    productView.setProducts(products);
}

init();
