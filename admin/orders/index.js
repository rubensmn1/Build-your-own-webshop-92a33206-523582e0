import Filters, { FILTER_TYPES } from '../../src/Filters';
import FilterButton from '../../src/Filters/FilterButton';
import OrderItem from '../../src/Order/OrderItem';
import OrderView from '../../src/Order/OrderView';
import OrderStorage from '../../src/Storage/OrderStorage';

// order elements
const orderTotalElement = document.getElementById('orderTotal');
const orderNumberElement = document.getElementById('orderNumber');

// get the product if from the url
const orderId = window.location.hash.slice(1);

// get the product from localstorage and create a Product class from the data
const order = OrderStorage.getById(orderId);

// used to filter
const formattedOrderItems = order.items.map(({ product, ...rest }) => ({
    ...product,
    ...rest,
    subtotal: rest.quantity * product.cost,
}));

const orderView = new OrderView(document.querySelector('tbody'), OrderItem);
orderView.setOrders(formattedOrderItems);

// setup the filter buttons
const filters = new Filters(document.getElementById('filters'));
const subtotalAscending = new FilterButton(
    `Subtotal<ion-icon name="chevron-down-outline"></ion-icon>`,
    'subtotal',
    FILTER_TYPES.NUMBER_ASCENDING,
);
const subtotalDescending = new FilterButton(
    `Subtotal<ion-icon name="chevron-up-outline"></ion-icon>`,
    'subtotal',
    FILTER_TYPES.NUMBER_DESCENDING,
);
const quantityAscending = new FilterButton(
    `Quantity<ion-icon name="chevron-down-outline"></ion-icon>`,
    'quantity',
    FILTER_TYPES.NUMBER_ASCENDING,
);
const quantityDescending = new FilterButton(
    `Quantity<ion-icon name="chevron-up-outline"></ion-icon>`,
    'quantity',
    FILTER_TYPES.NUMBER_DESCENDING,
);
const nameAscending = new FilterButton(
    `A-Z`,
    'name',
    FILTER_TYPES.STRING_ASCENDING,
);
const nameDescending = new FilterButton(
    `Z-A`,
    'name',
    FILTER_TYPES.STRING_DESCENDING,
);
filters.addButton([
    subtotalAscending,
    subtotalDescending,
    quantityAscending,
    quantityDescending,
    nameAscending,
    nameDescending,
]);

// set the title of the document
document.title += ` ${order.id}`;

// set the meta data to the dom
orderNumberElement.innerText = order.id;
orderTotalElement.innerText = `€${order.total}`;
