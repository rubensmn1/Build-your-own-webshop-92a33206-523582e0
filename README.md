# Web Trainee Eindproject

Dit is mijn uitwerking van de Web Trainee Eindproject opdracht. Hierin bouw ik een bestelapplicatie voor het aanbieden van eten / drinken. Ik zal dit project opleveren voor het **Certificaat mét Europese Studiepunten**.

In de applicatie kunnen artikelen worden gekocht door de gebruiker. De applicatie beschikt over:

- een overzicht van alle producten
- een specifieke pagina voor een product
- een winkelwagen
- een 'checkout' pagina met de bestelling

Ook is er een admin panel, hierin kan het volgende geregeld en gezien worden:

- overzicht van alle bestellingen
- overzicht van alle producten
- overzicht van een specifieke bestelling
- producten kunnen worden verwijderd, aangepast en aangemaakt worden

De applicatie is [hier online](https://webshop-bit.netlify.app/) te vinden.

## Installatie

Clone het project naar een lokale omgeving met [git](https://git-scm.com/) via de terminal

```sh
git clone git@bitlab.bit-academy.nl:9c7485c7-a1a8-11ea-a924-cec41367f4e7/76ad6b28-d0be-4fc3-855b-95d6f56b45eb/Build-your-own-webshop-92a33206-523582e0.git

cd Build-your-own-webshop-92a33206-523582e0
```

Installeer de nodige packages met je favoriete package manager.

```sh
yarn
# of
npm install
```

Gebruik het volgende command om de applicatie te starten

```sh
yarn dev
# of
npm run dev
```

## Reflectie

### Process

Het begin verliep vrij vlot, dit omdat ik een goed idee had voor de basis van de winkelwagen functionaliteiten en de localstorage. Hiervoor heb ik een standaard `LocalStorage` class gemaakt waar andere classes weer dingen van kunnen overnemen door 'inheritance' zoals de `ProductStorage`.

Met de gedachte dat ik een 'filter systeem' wilde maken voor het producten overzicht, was ik van plan om een soort 'view' te maken die regelde wat er hoe op het scherm terecht komt.

Ik had in het begin wat moeite met het maken van een herbruikbare 'component' om een product in het producten overzicht te laten zien, omdat ik deze component ook wilde gebruiken op de detail pagina van het product. Na een paar dingen geprobeerd te hebben kwam ik uit op een class die zelf de content van de HTML aanpast en interactie toevoeged. Deze kon ik dan weer aan een view aan laten maken waardoor het renderen van alle producten bijna vanzelf ging. Omdat de component zelfstandig is kon ik deze ook op de detail pagina gebruiken.

Daarna kwam de challenge om de filter 'events' te reflecteren in de producten view. Dit heb ik opgelost met het [Observer Pattern](https://refactoring.guru/design-patterns/observer) waardoor de views en componenten indirect met elkaar in verbinding staan. Om al deze events te beheren heb ik een EventManager gemaakt.

Toen de gebruikers kant van de applicatie een heel eind was ben ik het admin panel gaan maken. Doordat ik allerlei 'bouwstenen' had; views, components en de EventManager kon ik veel hergebruiken en relatief snel het admin panel werkende krijgen.

Na het implementeren van de functionaliteit voor het verwijderen van een product vond ik een probleem met de render functionaliteit van de view. Als je de producten had gefilterd op voorraad en verwijderde een product resette het filter en moest je opnieuw filteren. Dit heb ik opgelost door in plaats van elke keer dat een filter veranderd de product component volledig opnieuw aan te maken worden deze nu 1 keer aan gemaakt wanneer de producten geregistreerd worden aan de view. Deze components worden dan opgeslagen in een [Map](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Map) onder het `id` van het product. Als een filter dan veranderd word er een nieuwe gefilterde array aangemaakt met de id's. Tijdens het renderen word aan de hand van die array de product components toevoeged aan de view.

Verder ben ik bekend geworden met een voor mij nieuw HTML element de [dialog](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/dialog). Met de gedachte van semantic HTML is dit een handig element om te gebruiken waar basis functionaliteit aanhangt, denk aan `ESC` om de dialog te sluiten.

### Eindproduct

De web app is 'mobile first' gemaakt voor de gebruikers kant. Ik ben tevreden met de functionaliteiten en de UI van het eindproduct, omdat alles in het [plan van aanpak](./docs/plan-van-aanpak.md) gerealiseerd is. Graag had ik nog wel een betere manier gevonden om de dialogs meer dynamisch / herbruikbaar te maken.

Waar ik nog verbeter punten voor de toekomst in zie zijn:

- als de winkelwagen 'focus' verliest word deze weer gesloten.
- eigen / local svg icons in plaats van de icons van [IonIcons](https://www.ionic.io/ionicons), dit om ervoor te zorgen dat de icons sneller laden.
- een custom favicon om een betere representatie te geven van de webshop.
