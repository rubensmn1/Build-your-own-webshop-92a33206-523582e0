import ProductStorage from './src/Storage/ProductStorage';
import CartView from './src/Cart/CartView';
import ProductView from './src/Product/ProductView';
import Filters, { FILTER_TYPES } from './src/Filters';
import FilterButton from './src/Filters/FilterButton';
import CartIndicator from './src/Cart/CartIndicator';
import CheckoutButton from './src/Order/CheckoutButton';

const productList = document.getElementById('product-list');
const shoppingCartToggle = document.getElementById('shoppingCartToggle');
const cartOverlay = document.getElementById('cartOverlay');
const cartList = document.getElementById('cartList');
const cartElement = document.getElementById('cart');
const cartIndictatorElement = document.getElementById('cartIndicator');

const filtersContainer = document.getElementById('filters');

const cartIndicator = new CartIndicator(cartIndictatorElement);

const cartView = new CartView(cartList);
const checkoutButton = new CheckoutButton(
    document.getElementById('checkoutButton'),
);

const productView = new ProductView(productList);

const filters = new Filters(filtersContainer);
const priceAscending = new FilterButton(
    `Price<ion-icon name="chevron-down-outline"></ion-icon>`,
    'cost',
    FILTER_TYPES.NUMBER_ASCENDING,
);
const priceDescending = new FilterButton(
    `Price<ion-icon name="chevron-up-outline"></ion-icon>`,
    'cost',
    FILTER_TYPES.NUMBER_DESCENDING,
);
const nameAscending = new FilterButton(
    `A-Z`,
    'name',
    FILTER_TYPES.STRING_ASCENDING,
);
const nameDescending = new FilterButton(
    `Z-A`,
    'name',
    FILTER_TYPES.STRING_DESCENDING,
);
filters.addButton([
    priceAscending,
    priceDescending,
    nameAscending,
    nameDescending,
]);

cartElement.addEventListener('click', (e) => {
    e.stopPropagation();
});

cartOverlay.addEventListener('click', () => {
    cartOverlay.classList.add('hidden');
    document.body.classList.remove('overflow-hidden');
    document.body.classList.remove('md:overflow-auto');
});

shoppingCartToggle.addEventListener('click', () => {
    if (cartOverlay.classList.contains('hidden')) {
        cartOverlay.classList.remove('hidden');
        document.body.classList.add('overflow-hidden');
        document.body.classList.add('md:overflow-auto');
    } else {
        cartOverlay.classList.add('hidden');
        document.body.classList.remove('overflow-hidden');
        document.body.classList.remove('md:overflow-auto');
    }
});

async function init() {
    // get products from local storage
    let products = ProductStorage.get();
    // if there are no products in local storage fetch from json file
    if (products.length < 1) {
        products = await ProductStorage.reset();
        ProductStorage.set(products);
    }

    productView.setProducts(products);
}

init();
