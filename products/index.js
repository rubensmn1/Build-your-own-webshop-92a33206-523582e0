import ProductStorage from '../src/Storage/ProductStorage';
import Product from '../src/Product/ProductItem';
import CartIndicator from '../src/Cart/CartIndicator';
import CartView from '../src/Cart/CartView';
import CheckoutButton from '../src/Order/CheckoutButton';

// cart elements
const shoppingCartToggle = document.getElementById('shoppingCartToggle');
const cartOverlay = document.getElementById('cartOverlay');
const cartList = document.getElementById('cartList');
const cartElement = document.getElementById('cart');
const cartIndictatorElement = document.getElementById('cartIndicator');
// product container
const productContainer = document.querySelector('main');

const cartIndicator = new CartIndicator(cartIndictatorElement);
const cartView = new CartView(cartList);
const checkoutButton = new CheckoutButton(
    document.getElementById('checkoutButton'),
);

// get the product if from the url
const productId = window.location.hash.slice(1);

// get the product from localstorage and create a Product class from the data
const product = new Product(
    productContainer,
    ProductStorage.getItemById(productId),
);
product.render();

// set the title of the document
document.title += ` ${product.name}`;

// cart toggle logic
cartElement.addEventListener('click', (e) => {
    e.stopPropagation();
});

cartOverlay.addEventListener('click', () => {
    cartOverlay.classList.add('hidden');
    document.body.classList.remove('overflow-hidden');
    document.body.classList.remove('md:overflow-auto');
});

shoppingCartToggle.addEventListener('click', () => {
    if (cartOverlay.classList.contains('hidden')) {
        cartOverlay.classList.remove('hidden');
        document.body.classList.add('overflow-hidden');
        document.body.classList.add('md:overflow-auto');
    } else {
        cartOverlay.classList.add('hidden');
        document.body.classList.remove('overflow-hidden');
        document.body.classList.remove('md:overflow-auto');
    }
});
