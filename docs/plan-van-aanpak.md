# Plan van aanpak

## Wat voor webshop ga je maken?

Een bestelapplicatie voor het online bestellen van eten / drinken.

## Welke functionaliteiten biedt jouw webshop?

### Gebruiker/Klant

- Landing pagina met overzicht van alle producten
- Sorteren op **naam** en **prijs**
- Product detail pagina
- Winkelwagen drawer
- Bestel overzicht pagina

### Admin

- Overview van alle orders
- Overview van alle producten
- Sorteren op **naam**, **bedrag** en **hoeveelheid**
- Aanmaken van nieuwe producten
- Verwijderen van producten
- Wijzigingen van producten
- Producten kunnen 'resetten' naar de originele waarden

## Hoe gaat je webshop eruit zien?

### Gebruiker/Klant

![user wireframes and flow](./mock-wireflow-User.drawio.svg)

### Admin

![admin wireframes and flow](./mock-wireflow-Admin.drawio.svg)

## Code Languages / Tools

- HTML
- CSS
- JavaScript
- Tailwind CSS

De wireframes / flowcharts zijn gemaakt met [diagrams.net](https://app.diagrams.net/).

De mockdata is afkomstig van [Mockaroo](https://mockaroo.com/), dit is een handige website voor het maken van 'custom' mock data.
